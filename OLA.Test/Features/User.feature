﻿Feature:Users Resource
		In order to perform operations related to rides
		An application should do that for me

Scenario:To get all users
Given I am a client
When I make a GET Request to '/users'
Then The response status code is'200'
And The result should look like '[{"id":1,"name":"Chandler Bing","sex":1,"dob":"1957-12-20T00:00:00","email":"chandlerbing@gmail.com"},{"id":2,"name":"Monica Geller","sex":2,"dob":"1957-12-20T00:00:00","email":"monicageller@gmail.com"},{"id":3,"name":"Rachel Green","sex":2,"dob":"1957-12-20T00:00:00","email":"rachelgreen@gmail.com"}]'
