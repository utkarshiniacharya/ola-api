﻿Feature:Rides Resource
		In order to perform operations related to rides
		An application should do that for me

Scenario:To get all rides
Given I am a client
When I make a GET Request to '/rides'
Then The response status code is'200'
And The result should look like '[{"id":1,"requestId":2,"driverId":3,"status":"completed"},{"id":2,"requestId":3,"driverId":4,"status":"not completed"},{"id":3,"requestId":4,"driverId":6,"status":"completed"},{"id":4,"requestId":5,"driverId":1,"status":"not completed"}]'

Scenario:To get all requests of a particular user
Given I am a client
And The Rides table looks like
| Id | RequestId | DriverId | Status        |
| 1  | 2         | 3        | completed     |
| 2  | 3         | 4        | not completed |
| 3  | 4         | 6        | completed     |
| 4  | 5         | 1        | not completed |
And The Requests table looks like
| Id | UserId | Source          | Destination          | ConfirmationStatus |
| 1  | 1      | source address  | destination address  | not confirmed      |
| 2  | 2      | source address1 | destination address1 | confirmed          |
| 3  | 2      | source address2 | destination address2 | confirmed          |
| 4  | 13     | source address  | destination address  | confirmed          |
| 5  | 2      | source address  | destination address  | confirmed          |
When I make a GET Request to 'users/2/rides'
Then The response status code is '200'
And The result should look like '[{"id":1,"requestId":2,"driverId":3,"status":"completed"},{"id":2,"requestId":3,"driverId":4,"status":"not completed"},{"id":3,"requestId":4,"driverId":6,"status":"completed"},{"id":4,"requestId":5,"driverId":1,"status":"not completed"}]'

Scenario:To get details of a particular ride
Given I am a client
When I make a GET Request to 'users/2/rides/1'
Then The response status code is '200'
And The result should look like '{"id":1,"requestId":2,"driverId":3,"status":"completed"}'


Scenario:To create a new ride
Given I am a client
And The Rides table looks like
| Id | RequestId | DriverId | Status        |
| 1  | 2         | 3        | completed     |
| 2  | 3         | 4        | not completed |
| 3  | 4         | 6        | completed     |
| 4  | 5         | 1        | not completed |
And I have a Request object as '{"requestId":1, "driverId":3, "status":"not completed"}'
When I make a POST Request to 'drivers/2/requests/1/rides'
Then The Rides table should be like
| Id | RequestId | DriverId | Status        |
| 1  | 2         | 3        | completed     |
| 2  | 3         | 4        | not completed |
| 3  | 4         | 6        | completed     |
| 4  | 5         | 1        | not completed |
| 5  | 1         | 3        | not completed |

Scenario:To update Status of a Ride
Given I am a client
And The Rides table looks like
| Id | RequestId | DriverId | Status        |
| 1  | 2         | 3        | completed     |
| 2  | 3         | 4        | not completed |
| 3  | 4         | 6        | completed     |
| 4  | 5         | 1        | not completed |
| 5  | 1         | 3        | not completed |
And I have a Ride object as '{"requestId":5,"status":"completed"}'
When I make a PATCH Request to 'drivers/3/rides/5/updatestatus'
Then The Rides table should be like
| Id | RequestId | DriverId | Status        |
| 1  | 2         | 3        | completed     |
| 2  | 3         | 4        | not completed |
| 3  | 4         | 6        | completed     |
| 4  | 5         | 1        | not completed |
| 5  | 1         | 3        | completed     |

