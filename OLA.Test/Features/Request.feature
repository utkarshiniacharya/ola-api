﻿Feature:Requests Resource
		In order to perform operations related to requests
		An application should do that for me

Scenario:To get all requests
Given I am a client
When I make a GET Request to '/requests'
Then The response status code is'200'
And The result should look like '[{"id":1, "userId":1, "source":"source address", "destination":"destination address","confirmationStatus":"not confirmed"},{"id":2, "userId":2, "source":"source address1", "destination":"destination address1","confirmationStatus":"confirmed"},{"id":3, "userId":2, "source":"source address2", "destination":"destination address2", "confirmationStatus":"confirmed"},{"id":4, "userId":13, "source":"source address", "destination":"destination address","confirmationStatus":"confirmed"},{"id":5, "userId":2, "source":"source address", "destination":"destination address","confirmationStatus":"confirmed"}]'

Scenario:To get all requests of a particular user
Given I am a client
And The Requests table looks like
| Id | UserId | Source          | Destination          | ConfirmationStatus |
| 1  | 1      | source address  | destination address  | not confirmed      |
| 2  | 2      | source address1 | destination address1 | confirmed          |
| 3  | 2      | source address2 | destination address2 | confirmed          |
| 4  | 13     | source address  | destination address  | confirmed          |
| 5  | 2      | source address  | destination address  | confirmed          |
And I have a user id as '2'
When I make a GET Request to 'users/2/requests'
Then The response status code is '200'
And The result should look like '[{"id":2, "userId":2, "source":"source address1", "destination":"destination address1", "confirmationStatus":"confirmed"},{"id":3, "userId":2, "source":"source address2", "destination":"destination address2", "confirmationStatus":"confirmed"}]'

Scenario:To create a new request
Given I am a client
And The Requests table looks like
| Id | UserId | Source          | Destination          | ConfirmationStatus |
| 1  | 1      | source address  | destination address  | not confirmed      |
| 2  | 2      | source address1 | destination address1 | confirmed          |
| 3  | 2      | source address2 | destination address2 | confirmed          |
| 4  | 13     | source address  | destination address  | confirmed          |
| 5  | 2      | source address  | destination address  | confirmed          |
And I have a Request object as '{"userId":3, "source":"Banashankari", "destination":"HSR Layout", "confirmationStatus":"not confirmed"}'
When I make a POST Request to 'users/2/requests'
Then The Requests table should be like
| Id | UserId | Source          | Destination          | ConfirmationStatus |
| 1  | 1      | source address  | destination address  | not confirmed      |
| 2  | 2      | source address1 | destination address1 | confirmed          |
| 3  | 2      | source address2 | destination address2 | confirmed          |
| 4  | 13     | source address  | destination address  | confirmed          |
| 5  | 2      | source address  | destination address  | confirmed          |
| 6  | 3      | Banashankari    | HSR Layout           | not confirmed      |

Scenario:To update ConfirmationStatus of a Request
Given I am a client
And The Requests table looks like
| Id | UserId | Source          | Destination          | ConfirmationStatus |
| 1  | 1      | source address  | destination address  | not confirmed      |
| 2  | 2      | source address1 | destination address1 | confirmed          |
| 3  | 2      | source address2 | destination address2 | confirmed          |
| 4  | 13     | source address  | destination address  | confirmed          |
| 5  | 2      | source address  | destination address  | confirmed          |
| 6  | 3      | Banashankari    | HSR Layout           | not confirmed      |
And I have a Request object as '{"confirmationStatus":"confirmed"}'
When I make a PATCH Request to 'drivers/3/requests/4/confirmrequest'
Then The Requests table should be like
| Id | UserId | Source          | Destination          | ConfirmationStatus |
| 1  | 1      | source address  | destination address  | not confirmed      |
| 2  | 2      | source address1 | destination address1 | confirmed          |
| 3  | 2      | source address2 | destination address2 | confirmed          |
| 4  | 13     | source address  | destination address  | confirmed          |
| 5  | 2      | source address  | destination address  | confirmed          |
| 6  | 3      | Banashankari    | HSR Layout           | confirmed          |






