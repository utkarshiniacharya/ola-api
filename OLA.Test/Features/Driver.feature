﻿Feature:Drivers Resource
		In order to perform operations related to drivers
		An application should do that for me

Scenario:To get all drivers
Given I am a client
When I make a GET Request to '/drivers'
Then The response status code is'200'
And The result should look like '[{"id":1,"name":"Ross Geller","sex":1,"dob":"1957-12-20T00:00:00","email":"rossgeller@gmail.com"},{"id":2,"name":"Phoebe Buffay","sex":2,"dob":"1957-12-20T00:00:00","email":"phoebebuffay@gmail.com"},{"id":3,"name":"Joey Tribbiani","sex":1,"dob":"1957-12-20T00:00:00","email":"joeytribbiani@gmail.com"}]'
