﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OLA.Models.ResponseModel
{
    public class RideResponse
    {
        public int Id { get; set; }
        public int RequestId { get; set; }
        public int DriverId { get; set; }
        public string Status { get; set; }
    }
}
