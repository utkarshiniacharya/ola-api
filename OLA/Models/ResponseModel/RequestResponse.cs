﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OLA.Models.ResponseModel
{
    public class RequestResponse
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public string ConfirmationStatus { get; set; }
    }
}
