﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OLA.Models.Enum
{
    public enum Sex
    {
        Male=1,
        Female=2,
        NotProvided = 3
    }
}
