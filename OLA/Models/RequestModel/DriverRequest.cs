﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.Enum;

namespace OLA.Models.RequestModel
{
    public class DriverRequest
    {
        public string Name { get; set; }
        public Sex Sex { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
    }
}
