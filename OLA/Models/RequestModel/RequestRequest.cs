﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OLA.Models.RequestModel
{
    public class RequestRequest
    {
        public string Source { get; set; }
        public string Destination { get; set; }
        public string ConfirmationStatus { get; set; }
    }
}
