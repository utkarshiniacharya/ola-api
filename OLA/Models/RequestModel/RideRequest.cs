﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OLA.Models.RequestModel
{
    public class RideRequest
    {
        public int DriverId { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public string Status { get; set; }
    }
}
