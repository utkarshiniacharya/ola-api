﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OLA.Models.ResponseModel;
using OLA.Services;

namespace OLA.Controllers
{
    [ApiController]
    public class RidesController : ControllerBase
    {
        private readonly IRideService _rideService;
        public RidesController(IRideService rideService)
        {
            _rideService = rideService;
        }

        [HttpGet]
        [Route("rides")]
        public IActionResult Get()
        {
            var rides = _rideService.Get();
            return new JsonResult(rides);
        }

        [HttpGet("{userId}")]
        [Route("users/{userId}/rides")]
        public IActionResult Get(int userId)
        {
            var rides = _rideService.GetByUserId(userId);
            return new JsonResult(rides);
        }

        //[Route("users/{userId:int}/rides/{id}")]
        //public IActionResult Get(int id, int userId)
        //{
        //    return new JsonResult($"userId:{userId}, rideId:{id}");
        //}

        [HttpPost]
        [Route("drivers/{driverId:int}/requests/{requestId:int}/rides")]
        public IActionResult Post([FromBody] RideResponse ride, int driverId, int requestId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _rideService.Post(ride, driverId,requestId);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPatch]
        [Route("drivers/{driverId:int}/rides/{rideId:int}/updatestatus")]
        public IActionResult UpdateStatus(int driverId,int rideId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _rideService.Patch(driverId, rideId);
            return StatusCode(StatusCodes.Status201Created);
        }
    }
}
