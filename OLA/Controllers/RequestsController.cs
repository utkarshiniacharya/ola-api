﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OLA.Models.ResponseModel;
using OLA.Services;

namespace OLA.Controllers
{
    [ApiController]
    public class RequestsController : ControllerBase
    {
        private readonly IRequestService _requestService;
        public RequestsController(IRequestService requestService)
        {
            _requestService = requestService;
        }

        [HttpGet]
        [Route("requests")]
        public IActionResult Get()
        {
            var requests = _requestService.Get();
            return new JsonResult(requests);
        }

        [HttpGet("{userId}")]
        [Route("users/{userId}/requests")]
        public IActionResult Get(int userId)
        {
            var requests = _requestService.GetByUserId(userId);
            return new JsonResult(requests);
        }

        [HttpPost]
        [Route("users/{userId:int}/requests")]
        public IActionResult Post([FromBody] RequestResponse request,int userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _requestService.Post(request,userId);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPatch]
        [Route("drivers/{driverId:int}/requests/{requestId:int}/confirmrequest")]
        public IActionResult UpdateConfirmationStatus(int driverId, int requestId)
        {
            if (!ModelState.IsValid)
            {
                 return BadRequest(ModelState);
            }
            _requestService.Patch(driverId, requestId);
            return StatusCode(StatusCodes.Status201Created);
        }
    }
}
