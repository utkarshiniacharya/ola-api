﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OLA.Services;

namespace OLA.Controllers
{
    [ApiController]
    public class DriversController : ControllerBase
    {
        private readonly IDriverService _driverService;
        public DriversController(IDriverService driverService)
        {
            _driverService = driverService;
        }
        [HttpGet]
        [Route("drivers")]
        public IActionResult Get()
        {
            var users = _driverService.Get();
            return new JsonResult(users);
        }
    }
}
