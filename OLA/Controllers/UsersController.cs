﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OLA.Services;

namespace OLA.Controllers
{
    [ApiController]
    public class UsersController: ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        [Route("users")]
        public IActionResult Get()
        {
            var users = _userService.Get();
            return new JsonResult(users);
        }
    }
}
