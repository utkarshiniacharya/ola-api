﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;

namespace OLA.Services
{
    public interface IRequestService
    {
        List<RequestRequest> Get();
        List<RequestRequest> GetByUserId(int id);
        void Post(RequestResponse request, int userId);
        void Patch(int driverId, int requestId);
    }
}
