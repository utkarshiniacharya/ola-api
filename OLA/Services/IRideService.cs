﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;

namespace OLA.Services
{
    public interface IRideService
    {
        List<RideRequest> Get();
        List<RideRequest> GetByUserId(int id);
        void Post(RideResponse response, int driverId,int requestId);
        void Patch(int driverId, int rideId);
    }
}
