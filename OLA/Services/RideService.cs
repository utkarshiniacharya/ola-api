﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;
using OLA.Repositories;

namespace OLA.Services
{
    public class RideService : IRideService
    {
        private readonly IRideRepository _rideRepository;
        public RideService(IRideRepository rideRepository)
        {
            _rideRepository = rideRepository;
        }
        public List<RideRequest> Get()
        {
            return _rideRepository.Get().ToList();
        }
        public List<RideRequest> GetByUserId(int id)
        {
            return _rideRepository.GetByUserId(id).ToList();
        }

        public void Patch(int driverId, int rideId)
        {
            _rideRepository.Patch(driverId, rideId);
        }

        public void Post(RideResponse ride,int driverId, int requestId)
        {
            _rideRepository.Post(ride, driverId, requestId);
        }
    }
}
