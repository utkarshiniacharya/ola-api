﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.RequestModel;

namespace OLA.Services
{
    public interface IDriverService
    {
        List<DriverRequest> Get();
    }
}
