﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;
using OLA.Repositories;

namespace OLA.Services
{
    public class RequestService : IRequestService
    {
        private readonly IRequestRepository _requestRepository;
        public RequestService(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }
        public List<RequestRequest> Get()
        {
            return _requestRepository.Get().ToList();
        }
        public List<RequestRequest> GetByUserId(int id)
        {
            return _requestRepository.GetByUserId(id).ToList();
        }

        public void Patch(int driverId, int requestId)
        {
            _requestRepository.ConfirmRequest(driverId, requestId);
        }

        public void Post(RequestResponse request,int userId)
        {
            _requestRepository.Post(request,userId);
        }
    }
}
