﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Threading.Tasks;

namespace OLA.Repositories
{
    public class BaseRepository<T>
    {
        private readonly Connection _options;
        public BaseRepository(Connection connection)
        {
            _options = connection;
        }
        public IEnumerable<T> Get(string sqlQuery)
        {
            using (var connection = new SqlConnection(_options.DbConnection))
            {
                var all = connection.Query<T>(sqlQuery);
                return all;
            }
        }
        public IEnumerable<T> Get(string sqlQuery, object param)
        {
            using (var connection = new SqlConnection(_options.DbConnection))
            {
                var all = connection.Query<T>(sqlQuery, param);
                return all;
            }
        }
        public void Post(string sqlQuery, object param)
        {
            using (var connection = new SqlConnection(_options.DbConnection))
            {
                var all = connection.Execute(sqlQuery, param);
            }

        }
        public void Patch(string sqlQuery, object param)
        {
            using (var connection = new SqlConnection(_options.DbConnection))
            {
                var all = connection.Execute(sqlQuery, param);
            }

        }
        public void Put(string sqlQuery, object param)
        {
            using (var connection = new SqlConnection(_options.DbConnection))
            {
                var all = connection.Execute(sqlQuery, param);
            }

        }
        public void Delete(string sqlQuery, object param)
        {
            using (var connection = new SqlConnection(_options.DbConnection))
            {
                var all = connection.Execute(sqlQuery, param);
            }

        }
    }
}
