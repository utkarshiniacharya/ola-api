﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using OLA.Models.RequestModel;

namespace OLA.Repositories
{
    public class DriverRepository : BaseRepository<DriverRequest>, IDriverRepository
    {
        public DriverRepository(IOptions<Connection> options) : base(options.Value)
        {
            //CreateList();
        }
        public IEnumerable<DriverRequest> Get()
        {
            const string sql = @"
            SELECT Name
	            ,Sex
	            ,Dob AS DateOfBirth
	            ,Email
            FROM Drivers";
            return Get(sql);
        }
    }
}
