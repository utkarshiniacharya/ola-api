﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;

namespace OLA.Repositories
{
    public class RideRepository: BaseRepository<RideRequest>, IRideRepository
    {
        public RideRepository(IOptions<Connection> options) : base(options.Value)
        {
            //CreateList();
        }
        public IEnumerable<RideRequest> Get()
        {
            const string sql = @"
            SELECT R1.DriverId
	            ,R2.Source
	            ,R2.Destination
	            ,R1.STATUS
            FROM Rides R1
	            ,Requests R2
            WHERE R1.RequestId = R2.Id";
            return Get(sql);
        }
        public IEnumerable<RideRequest> GetByUserId(int id)
        {
            const string sql = @"
            SELECT R1.DriverId
	            ,R2.Source
	            ,R2.Destination
	            ,R1.STATUS
            FROM Rides R1
	            ,Requests R2
            WHERE R1.RequestId = R2.Id
	            AND R2.UserId = @id";
            return Get(sql,new { id });
        }
        public void Post(RideResponse ride, int driverId,int requestId)
        {
            const string sql = @"
            INSERT INTO Rides
            VALUES (
	            @RequestId
	            ,@DriverId
	            ,@Status
	            )";
            base.Post(sql, new
            {
                RequestId=requestId,
                DriverId=driverId,
                Status = "not completed"
            });
        }
        public void Patch(int driverId,int rideId)
        {
            String Status = "completed";
            const string sql = @"
            UPDATE Rides
            SET Status=@Status
            WHERE Id = @rideId";
            base.Patch(sql, new
            {
                rideId,
                Status
            });
        }
    }
}
