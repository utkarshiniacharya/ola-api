﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;

namespace OLA.Repositories
{
    public class UserRepository : BaseRepository<UserRequest>, IUserRepository
    {
        public UserRepository(IOptions<Connection> options) : base(options.Value)
        {
            //CreateList();
        }
        public IEnumerable<UserRequest> Get()
        {
            const string sql = @"
            SELECT Name
	            ,Sex
	            ,Dob AS DateOfBirth
	            ,Email
            FROM Users";
            return Get(sql);
        }
    }
}
