﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;

namespace OLA.Repositories
{
    public class RequestRepository : BaseRepository<RequestRequest>, IRequestRepository
    {
        public RequestRepository(IOptions<Connection> options) : base(options.Value)
        {
            //CreateList();
        }
        public IEnumerable<RequestRequest> Get()
        {
            const string sql = @"
            SELECT Source
	            ,Destination
	            ,ConfirmationStatus
            FROM Requests";
            return Get(sql);
        }
        public IEnumerable<RequestRequest> GetByUserId(int userId)
        {
            const string sql = @"
            SELECT Source
	            ,Destination
	            ,ConfirmationStatus
            FROM Requests
            WHERE UserId = @userId";
            return Get(sql, new { userId });
        }
        public void Post(RequestResponse request,int userId)
        {
            const string sql = @"
            INSERT INTO Requests
            VALUES (
	            @UserId
	            ,@Source
	            ,@Destination
	            ,@ConfirmationStatus
	            )";
            base.Post(sql, new
            { 
                UserId=userId,
                Source=request.Source,
                Destination=request.Destination,
                ConfirmationStatus="not confirmed"
            });
        }
        public void ConfirmRequest(int driverId,int requestId)
        {
            String ConfirmationStatus = "confirmed";
            const string sql = @"
            UPDATE Requests
            SET ConfirmationStatus=@ConfirmationStatus
            WHERE Id = @requestId";
            base.Patch(sql, new
            {
                requestId,
                ConfirmationStatus
            });
        }
    }
}
