﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;

namespace OLA.Repositories
{
    public interface IRideRepository
    {
        IEnumerable<RideRequest> Get();
        IEnumerable<RideRequest> GetByUserId(int id);
        public void Post(RideResponse ride, int driverId,int requestId);
        public void Patch(int driverId, int rideId);
    }
}
