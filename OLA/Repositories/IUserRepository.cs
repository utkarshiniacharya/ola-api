﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.RequestModel;

namespace OLA.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<UserRequest> Get();
    }
}
