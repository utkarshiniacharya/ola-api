﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLA.Models.RequestModel;
using OLA.Models.ResponseModel;

namespace OLA.Repositories
{
    public interface IRequestRepository
    {
        IEnumerable<RequestRequest> Get();
        IEnumerable<RequestRequest> GetByUserId(int userId);
        public void Post(RequestResponse request,int userId);
        public void ConfirmRequest(int driverId, int requestId);
    }
}
